'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')

const createToken = (user) => {
    try {
        const payload = {
            sub: user._id,
            username: user.username,
            email: user.email,
            name: user.name,
            surname: user.surname,
            role: user.role,
            iat: moment().unix(),
            exp: moment().add(30, 'days').unix()
        }
        return jwt.encode(payload, process.env.JWT_SECRET)
    } catch (error) {
        console.log('ERROR WHEN GENERATING TOKEN', error)
    }
}

module.exports = { createToken }