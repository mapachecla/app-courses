'use strict'

const express = require('express')
const multipart = require('connect-multiparty')
const CourseController = require('../../controllers/v1/course-controller')
const md_auth = require('../../middlewares/auth')

var md_upload = multipart({ uploadDir: './uploads/courses' })

const route = express.Router()

route.post('/register-course', md_auth.ensureAuth, md_auth.isProf, CourseController.saveCourse)
route.get('/get-course/:id', md_auth.ensureAuth, CourseController.getCourse)
route.get('/get-courses/:professor?', md_auth.ensureAuth, CourseController.getCourses)
route.put('/update-course/:id', md_auth.ensureAuth, md_auth.isProf, CourseController.updateCourse)
route.delete('/delete-course/:id', md_auth.ensureAuth, md_auth.isProf, CourseController.deleteCourse)
route.post('/upload-image-course/:id', [md_auth.ensureAuth, md_upload], md_auth.isProf, CourseController.uploadImage)



module.exports = route