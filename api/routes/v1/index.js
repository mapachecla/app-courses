const app = require('../../app')
const authRoutes = require('./auth-route')
const userRoutes = require('./user-route')
const courseRoutes = require('./course-route')


module.exports = app => {
    app.use('/api/v1/users', authRoutes, userRoutes)
    app.use('/api/v1/courses', courseRoutes)
}