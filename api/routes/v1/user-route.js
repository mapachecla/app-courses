const express = require('express')
const userController = require('../../controllers/v1/user-controller')
const testController = require('../../controllers/v1/test-publit')
const md_auth = require('../../middlewares/auth')
const multipart = require('connect-multiparty')

var md_upload = multipart({ uploadDir: './uploads/users' })

const route = express.Router()

//-- 3 end points para crear usuarios de test --//
route.post('/register/s', userController.saveStudent)
route.post('/register/p', userController.saveProfessor)
route.post('/register-admin', userController.saveAdmin)

route.put('/update-user/:id', md_auth.ensureAuth, userController.updateUser)
route.post('/delete-user/:id', md_auth.ensureAuth, userController.deleteUser)
route.get('/get-users', md_auth.ensureAuth, md_auth.isAdmin, userController.getUsers)
route.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], userController.uploadImage)
route.get('/get-image-user/:imageFile', userController.getImageFile)

// --- Para usar con publitio --- //
route.get('/upload-file', testController.upFile)
route.get('/get-files', testController.getFiles)



module.exports = route