const express = require('express')

const authController = require('../../controllers/v1/auth-controller')

const route = express.Router()

route.get('/prueba', authController.prueba)

route.post('/signup/s', authController.signupStudent)
route.post('/email-activate/s/:token', authController.activateAccStudent)
route.post('/signup/p', authController.signupProfessor)
route.post('/email-activate/p/:token', authController.activateAccProfessor)

route.put('/forgot-password', authController.forgotPassword)
route.put('/reset-password/:token', authController.resetPassword)
route.post('/login', authController.login)




module.exports = route