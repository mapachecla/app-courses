// --- Controlador para usar con Publit.io --- //

const PublitioAPI = require('publitio_js_sdk').default
const fs = require('fs')
const publitio = new PublitioAPI(process.env.PUBLITIO_APYKEY, process.env.PUBLITIO_APYSECRET)



const upFile = (req, res) => {

    const file = fs.readFileSync('C:\\Users\\Claudio\\Pictures\\imagenes-fruta\\cerati.jpg')

    publitio.uploadFile(file, 'file', {
            title: 'Bichito',
            description: 'Bichito celeste',
            tags: 'go, golang, bichito',
            privacy: '1',
            option_download: '1'
        })
        .then((data) => {
            console.log(data),
                res.status(200).send({ message: 'ARCHIVO SUBIDO CON EXITO!!' })
        })
        .catch((error) => {
            console.log(error)
            res.status(400).send({ error: 'ERROR AL SUBIR EL ARCHIVO' })
        })

}


const getFiles = (req, res) => {

    publitio.call('/files/list', 'GET', { offset: '0', limit: '10' })
        .then((data) => {
            console.log(data),
                res.status(200).send({ message: data })
        }).catch((error) => {
            console.log(error),
                res.status(400).send({ error: error, message: 'ERROR AL LISTAR CONTENIDO' })
        })

}



module.exports = {
    upFile,
    getFiles
}