'use strict'

const bcrypt = require('bcrypt')
const User = require('../../models/user-model')
const jwt = require('../../service/jwt')
const jwtoken = require('jsonwebtoken')
const _ = require('lodash')
const mailgun = require("mailgun-js");
const mg = mailgun({ apiKey: process.env.MAILGUN_APIKEY, domain: process.env.DOMAIN_MAILGUN });

const prueba = (req, res) => {
    res.status(200).send({ message: 'Controlador de prueba plataforma curso OK' })
}

// --------------------------------------------------------- STUDENTS ---------------------------------------------------------//
const signupStudent = (req, res) => {
    try {
        const { username, email, password, password2 } = req.body

        if (username && email && password && password2) {

            if (password === password2) {

                if (password.length > 8) {

                    // Buscar si existe el usuario
                    User.findOne({ email }).exec((err, user) => {
                        if (user) {
                            return res.status(400).json({ error: "email ya registrado, ingrese otro correo" })
                        } else {
                            const token = jwtoken.sign({ username, email, password }, process.env.JWT_ACC_ACTIVATE, { expiresIn: '20m' })
                            const url_client = process.env.CLIENT_URL
                            const output = `
                            <h2>Por favor haga click en el siguiente enlace para activar su cuenta</h2>
                            <p>${url_client}/auth/activate/${token}</p>
                            <a href="${url_client}/auth/activate/${token}"><button>Activa tu cuenta</button></a>
                            `
                            console.log(username, email, password)

                            const data = {
                                from: 'noreply@procyon.com',
                                to: email,
                                subject: 'Activá tu cuenta ✔',
                                generateTextFromHTML: true,
                                html: output
                            };
                            mg.messages().send(data, function(err, body) {
                                console.log(body);
                                if (err) {
                                    return res.json({
                                        error: err.message
                                    })
                                }
                                console.log(`token de activación: ${token}`)
                                return res.json({ message: 'El email se ha enviado correctamente, revisa tu cuenta para activar tu usuario' })
                            });
                        }
                    })

                } else {
                    res.status(400).send({ message: 'PASS MUY CORTO' })
                }

            } else {
                res.status(400).send({ message: 'PASS NO SON IGUALES' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }
    } catch (e) {
        console.log('Error de servidor', e)
        res.status(500).send({ message: e.message })
    }

}

const activateAccStudent = (req, res) => {
    // -- elegir si voy a pasar el token por url o por body
    //const { token } = req.body
    const { token } = req.params
    console.log("lo que se pasa por param es : ", req.params)

    if (token) {
        jwtoken.verify(token, process.env.JWT_ACC_ACTIVATE, function(err, decodeToken) {
            if (err) {
                return res.json({ error: 'Incorrect or Expired token' })
            }
            const { username, email, password } = decodeToken
            User.findOne({ email }).exec((err, user) => {
                if (user) {
                    res.json({ error: 'Email ya registrado' })
                } else {
                    async function saveUser() {
                        bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err
                                bcrypt.hash(decodeToken.password, salt, (err, hash) => {
                                    if (err) throw err
                                    decodeToken.password = hash
                                    const password = decodeToken.password
                                    const role = 'ROLE_STU'
                                    const newUser = new User({ username, email, password, role })
                                    newUser
                                        .save()
                                        .then(user => {
                                            res.status(200).send({ user: user })
                                        }).catch(err => console.log(err))
                                })
                            })
                            //--- SIN USAR SALT ---//
                            // const role = 'ROLE_STU'
                            // const hash = await bcrypt.hash(decodeToken.password, 15)
                            // decodeToken.password = hash
                            // const password = decodeToken.password
                            // const newUser = new User({ username, email, password, role })
                            // newUser
                            //     .save()
                            //     .then(user => {
                            //         res.status(200).send({ user: user })
                            //     }).catch(err => console.log(err))
                    }
                    saveUser()
                }
            })
        })

    } else {
        return res.json({ error: 'No se encuentra el token' })
    }
}

//--------------------------------------------------------- PROFESSOR --------------------------------------------------------//

const signupProfessor = (req, res) => {
    try {
        const {
            username,
            email,
            password,
            password2,
            name,
            surname,
            telefono,
            pais,
            provincia
        } = req.body

        if (username && email && password && password2 && name && surname && telefono) {

            if (password === password2) {

                if (password.length > 8) {

                    // Buscar si existe el usuario
                    User.findOne({ email }).exec((err, user) => {
                        if (user) {
                            return res.status(400).json({ error: "email ya registrado, ingrese otro correo" })
                        } else {
                            const token = jwtoken.sign({ username, email, password, name, surname, telefono, pais, provincia }, process.env.JWT_ACC_ACTIVATE, { expiresIn: '20m' })
                            const url_client = process.env.CLIENT_URL
                            const output = `
                            <h2>Por favor haga click en el siguiente enlace para activar su cuenta</h2>
                            <p>${url_client}/auth/activate/${token}</p>
                            <a href="${url_client}/auth/activate/${token}"><button>Activa tu cuenta</button></a>
                            `
                            console.log(username, email, password, name, surname, telefono, pais, provincia)

                            const data = {
                                from: 'noreply@procyon.com',
                                to: email,
                                subject: 'Activá tu cuenta ✔',
                                generateTextFromHTML: true,
                                html: output
                            };
                            mg.messages().send(data, function(err, body) {
                                console.log(body);
                                if (err) {
                                    return res.json({
                                        error: err.message
                                    })
                                }
                                console.log(`token de activación: ${token}`)
                                return res.json({ message: 'Revisa tu cuenta para activar tu usuario' })
                            });
                        }
                    })

                } else {
                    res.status(400).send({ message: 'PASS MUY CORTO' })
                }

            } else {
                res.status(400).send({ message: 'PASS NO SON IGUALES' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }
    } catch (e) {
        console.log('Error de servidor', e)
        res.status(500).send({ message: e.message })
    }

}

const activateAccProfessor = (req, res) => {
    // -- elegir si voy a pasar el token por url o por body
    const { token } = req.params

    if (token) {
        jwtoken.verify(token, process.env.JWT_ACC_ACTIVATE, function(err, decodeToken) {
            if (err) {
                return res.json({ error: 'Incorrect or Expired token' })
            }
            const { username, email, password, name, surname, telefono, pais, provincia } = decodeToken
            User.findOne({ email }).exec((err, user) => {
                if (user) {
                    res.json({ error: 'Email ya registrado' })
                } else {
                    async function saveUser() {
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err) throw err
                            bcrypt.hash(decodeToken.password, salt, (err, hash) => {
                                if (err) throw err
                                decodeToken.password = hash
                                const password = decodeToken.password
                                const role = 'ROLE_PROF'
                                const newUser = new User({ username, email, password, name, surname, telefono, pais, provincia, role })
                                newUser
                                    .save()
                                    .then(user => {
                                        res.status(200).send({ user: user })
                                    }).catch(err => console.log(err))
                            })
                        })
                    }
                    saveUser()
                }
            })
        })

    } else {
        return res.json({ error: 'No se encuentra el token' })
    }
}

//--------------------------------------------- RESET PASSWORD Y LOGIN -------------------------------------------------------//

const forgotPassword = (req, res) => {
    const { email } = req.body

    User.findOne({ email }, (err, user) => {
        if (err || !user) {
            return res.status(400).json({ error: 'No existe un usuario con éste email' })
        }
        const token = jwtoken.sign({ _id: user._id }, process.env.RESET_PASSWORD_KEY, { expiresIn: '20m' })
        const url_client = process.env.CLIENT_URL
        const output = `
        <h2>Por favor haga click en el siguiente enlace para resetear su contraseña</h2>
        <p>${url_client}/resetpassword/${token}</p>
        <a href="${url_client}/resetpassword/${token}"><button>Reset Pass</button></a>
        `
        console.log(email)

        const data = {
            from: 'noreply@procyon.com',
            to: email,
            subject: 'Resetear contraseña',
            generateTextFromHTML: true,
            html: output
        };

        return user.updateOne({ resetLink: token }, function(err, success) {
            if (err) {
                return res.status(400).json({ error: 'Error en el link para resetear contraseña' })
            } else {
                mg.messages().send(data, function(err, body) {
                    console.log(body);
                    if (err) {
                        return res.json({
                            error: err.message
                        })
                    }
                    console.log(`token de reset password: ${token}`)
                    return res.json({ message: 'El email se ha enviado correctamente, revisa tu cuenta para resetear tu password' })
                });
            }
        })
    })
}

const resetPassword = (req, res) => {

    // elegir por donde voy a pasar el token del resetLink, por url o por body
    const resetLink = req.params.token
    const { newPass, newPass2 } = req.body

    if (resetLink) {

        if (newPass && newPass2) {

            if (newPass === newPass2) {

                if (newPass.length > 8) {

                    jwtoken.verify(resetLink, process.env.RESET_PASSWORD_KEY, function(error, decodeData) {

                        if (error) {
                            return res.status(401).json({
                                error: 'Incorrect reset token or it is expired'
                            })
                        }

                        User.findOne({ resetLink }, (err, user) => {
                            if (err || !user) {
                                return res.status(400).json({ error: 'No existe el usuario con éste token' })
                            }
                            // Si está todo OK
                            function savePass() {
                                bcrypt.genSalt(10, (err, salt) => {
                                    if (err) throw err
                                    bcrypt.hash(newPass, salt, (err, hash) => {
                                        if (err) throw err

                                        const newPass = hash
                                        const obj = {
                                            password: newPass,
                                            resetLink: ''
                                        }

                                        user = _.extend(user, obj)
                                        user.save((err, result) => {
                                            if (err) {
                                                return res.status(400).json({ error: 'Error para resetear contraseña' })
                                            } else {
                                                return res.status(200).json({ message: 'PASSWORD ACTUALIZADO CON EXITO!!' })
                                            }
                                        })
                                    })
                                })
                            }
                            savePass()

                        })

                    })

                } else {
                    res.status(400).send({ message: 'PASS MUY CORTO LA CONCHA TUYA' })
                }

            } else {
                res.status(400).send({ message: 'LOS PASS NO SON IGUALES' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }

    } else {
        return res.status(401).json({ error: "Error de autenticación, no existe el token para resetear la pass" })
    }

}

const login = async(req, res) => {
    try {
        const { username, email, password } = req.body;
        const params = req.body

        if (password && email || username) {

            const user = await User.findOne({ email })
            if (user) {

                const isOK = await bcrypt.compare(password, user.password)
                if (isOK) {
                    if (params.gethash) {
                        console.log(user)
                        res.status(200).send({ token: jwt.createToken(user) })
                    } else {
                        console.log(user)
                        res.status(200).send({ message: 'LOGIN SUCCESS' })
                    }
                } else {
                    res.status(403).send({ message: 'INVALID_PASSWORD' })
                }

            } else {

                const user = await User.findOne({ username })
                if (user) {
                    const isOK = await bcrypt.compare(password, user.password)
                    if (isOK) {
                        if (params.gethash) {
                            console.log(user)
                            res.status(200).send({ token: jwt.createToken(user) })
                        } else {
                            console.log(user)
                            res.status(200).send({ message: 'LOGIN SUCCESS' })
                        }
                    } else {
                        res.status(403).send({ message: 'INVALID_PASSWORD' })
                    }
                } else {
                    res.status(401).send({ message: 'USER_NOT_FOUND' })
                }

            }
        } else {
            res.status(200).json({ message: 'INCOMPLETE DATA' })
        }

    } catch (e) {
        res.status(500).send({ error: 'ERROR', message: e.message })
    }
}


module.exports = {
    prueba,
    signupStudent,
    activateAccStudent,
    signupProfessor,
    activateAccProfessor,
    forgotPassword,
    resetPassword,
    login
}