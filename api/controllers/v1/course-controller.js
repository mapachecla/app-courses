'use strict'

const jwtoken = require('jsonwebtoken')
const path = require('path')
const fs = require('fs')
const mongoosePagination = require('mongoose-pagination')
const Course = require('../../models/course-model')
const Video = require('../../models/video-model')
const User = require('../../models/user-model')



const saveCourse = (req, res) => {
    try {

        const course = new Course()
        const params = req.body

        course.title = params.title
        course.about = params.about
        course.number_class = params.number_class
        course.number_hours = params.number_hours
        course.language = params.language
        course.subtitle = params.subtitle
        course.description = params.description
        course.price = params.price
        course.price_discount = params.price_discount
        course.image = 'null'
        course.platform = params.platform
        course.professor_description = params.professor_description
            //course.professor = params.professor

        // Saco ID de profesor directamente del token (revisar como tratarlo en el front luego, sino borro todo y descomento la linea 31 para pasar el id por parametro)
        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' })
        }
        const token = req.headers.authorization.replace(/['"']+/g, '')
        const payload = jwtoken.decode(token, process.env.JWT_SECRET)
        let professorId = payload.sub
        course.professor = professorId

        course.save((err, courseStored) => {
            if (err) {
                res.status(500).send({ error: 'SERVER ERROR' })
            } else {
                if (!courseStored) {
                    res.status(404).send({ error: 'NO SE HA PODIDO GUARDAR EL CURSO' })
                } else {
                    console.log(`Curso registrado exitosamente: ${courseStored}`)
                    res.status(200).send({ course: courseStored })
                }
            }
        })

    } catch (e) {
        res.status(500).send({ error: e.message })
    }
}

const getCourse = (req, res) => {
    try {

        const courseId = req.params.id

        Course.findById(courseId, (err, course) => {
            if (err) {
                res.status(500).send({ error: 'SERVER ERROR EN LA PETICIÓN DEL CURSO' })
            } else {
                if (!course) {
                    res.status(404).send({ message: 'NO EXISTE EL CURSO' })
                } else {
                    res.status(200).send({ course: course })
                }
            }
        })

    } catch (e) {
        res.status(500).send({ error: e.message })
    }

}

const getCourses = (req, res) => {
    try {

        const professorId = req.params.professor;

        if (!professorId) {
            var find_course = Course.find({}).sort('title')
        } else {
            var find_course = Course.find({ professor: professorId }).sort('title')
        }

        find_course.populate({
                path: 'professor',
                select: '_id'
            })
            .exec((err, courses) => {
                if (err) {
                    res.status(500).send({ status: 'ERROR', message: 'ERROR EN LA PETICION ' });
                } else {
                    if (!courses) {
                        res.status(404).send({ status: 'ERROR', message: 'NO SE ENCONTRARON CURSOS ' });
                    } else {
                        res.status(200).send({ courses: courses })
                    }
                }
            })

    } catch (e) {
        res.send(500).send({ error: e.message, message: 'SERVER ERROR' })
    }
}

const updateCourse = (req, res) => {
    try {

        const courseId = req.params.id;
        const update = req.body;

        Course.findByIdAndUpdate(courseId, update, (err, courseUpdated) => {
            if (err) {
                res.status(500).send({ error: 'SERVER ERROR EN LA PETICIÓN' })
            } else {
                if (!courseUpdated) {
                    res.status(404).send({ error: 'NO SE PUDO ACCEDER AL CURSO' })
                } else {
                    res.status(200).send({ course: courseUpdated })
                }
            }
        })

    } catch (e) {
        res.send(500).send({ error: e.message, message: 'SERVER ERROR' })
    }
}

const deleteCourse = (req, res) => {
    try {

        const courseId = req.params.id;

        Course.findByIdAndRemove(courseId, (err, courseRemoved) => {
            if (err) {
                res.status(500).send({ status: 'ERROR', message: 'ERROR AL ELIMINAR CURSO' })
            } else {
                if (!courseRemoved) {
                    res.status(404).send({ status: 'ERROR', message: 'NO SE ENCUENTRA EL CURSO' })
                } else {
                    Video.find({ course: courseRemoved._id }).remove((err, videoRemoved) => {
                        if (err) {
                            res.status(500).send({ status: 'ERROR', message: 'ERROR AL ELIMINAR VIDEO' })
                        } else {
                            if (!videoRemoved) {
                                res.status(404).send({ status: 'ERROR', message: 'NO SE HA PODIDO ELIMINAR EL VIDEO' })
                            } else {
                                res.status(200).send({ course: courseRemoved })
                            }
                        }
                    })
                }
            }
        })

    } catch (e) {
        res.send(500).send({ error: e.message, message: 'SERVER ERROR' })
    }
}

const uploadImage = (req, res) => {
    try {

        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' })
        }
        const token = req.headers.authorization.replace(/['"']+/g, '')
        const payload = jwtoken.decode(token, process.env.JWT_SECRET)
        console.log(`Usuario logueado: ${payload.sub}`)

        //Sacar si figura entre mis cursos el id de curso que subiré por parámetro


        const courseId = req.params.id
        const file_name = 'Imagen NO subida'

        if (req.files) {
            const file_path = req.files.image.path

            const file_split = file_path.split('\\')
            const file_name = file_split[2]

            const ext_split = file_name.split('\.')
            const file_ext = ext_split[1]

            if (file_ext === 'png' || file_ext === 'jpg' || file_ext === 'gif') {

                Course.findByIdAndUpdate(courseId, { image: file_name }, (err, courseUpdated) => {
                    if (err) {
                        res.status(500).send({ error: 'ERROR COURSE UPDATED' })
                    } else {
                        if (!courseUpdated) {
                            res.status(404).send({ message: 'NO SE PUDO ACTUALIZAR EL CURSO' })
                        } else {
                            console.log(`Se ha actualizado imagen en curso: ${courseId}`)
                            res.status(200).send({ image: file_name, course: courseUpdated })
                        }
                    }
                })

            } else {
                res.status(200).send({ message: 'EXTENSIÓN INVÁLIDA, SOLO SE ADMITE .png .jpg .gif' })
            }

        } else {
            res.status(200).send({ message: 'NO has subido ninguna imagen' })
        }

    } catch (e) {
        res.status(500).send({ message: 'ERROR AL SUBIR IMAGEN' })
        console.log('Error upload image: ', e.message)
    }
}


module.exports = {
    saveCourse,
    getCourse,
    getCourses,
    updateCourse,
    deleteCourse,
    uploadImage
}