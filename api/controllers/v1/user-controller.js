'use strict'
const User = require('../../models/user-model')
const jwt = require('../../service/jwt')
const bcrypt = require('bcrypt')
const fs = require('fs')
const path = require('path')


const updateUser = (req, res) => {
    try {

        const userId = req.params.id
        const update = req.body

        if (update.password == '' && update.password2 == '') {

            return res.json({ message: 'NO PASS VACIOOOOS' })

        } else {

            if (!update.password && !update.password2) {

                // --- Actualizo datos menos el password ---//
                if (userId != req.user.sub) {
                    return res.status(500).send({ error: 'ERROR', message: 'INVALID TOKEN' })
                }

                User.findByIdAndUpdate(userId, update, (err, userUpdate) => {
                    if (err) {
                        res.status(500).send({ error: 'ERROR', message: 'ERROR_USER_UPDATED' })
                    } else {
                        if (!userUpdate) {
                            res.status(404).send({ error: 'ERROR', message: 'NOT_USER_UPDATED' })
                        } else {
                            res.status(200).send({ user: userUpdate })
                        }
                    }
                })

            } else {

                if (update.password && update.password2) {
                    if (update.password === update.password2) {
                        const passw = update.password
                        if (passw.length > 8) {

                            bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err
                                bcrypt.hash(update.password, salt, (err, hash) => {
                                    if (err) throw err
                                    update.password = hash
                                    console.log(`La pass encriptada es: ${update.password}`)

                                    if (userId != req.user.sub) {
                                        return res.status(500).send({ error: 'ERROR', message: 'INVALID TOKEN' })
                                    }

                                    User.findByIdAndUpdate(userId, update, (err, userUpdate) => {
                                        if (err) {
                                            res.status(500).send({ error: 'ERROR', message: 'ERROR_USER_UPDATED' })
                                        } else {
                                            if (!userUpdate) {
                                                res.status(404).send({ error: 'ERROR', message: 'NOT_USER_UPDATED' })
                                            } else {
                                                res.status(200).send({ user: userUpdate })
                                            }
                                        }
                                    })

                                })
                            })

                        } else {
                            res.status(400).send({ message: 'PASS MUY CORTO' })
                        }

                    } else {
                        res.status(400).send({ message: 'PASS NO SON IGUALES' })
                    }

                } else {
                    res.status(400).send({ message: 'INCOMPLETE DATA' })
                }

            }
        }

    } catch (e) {
        res.status(500).send({ error: e, message: 'SERVER ERROR' })
    }
}

const deleteUser = async(req, res) => {
    try {

        const userId = req.params.id
        if (!userId) {
            return res.status(400).send({ error: 'NO SE ENCONTRÓ EL USER POR URL' })
        } else {
            await User.findByIdAndDelete(userId)
            res.status(200).send({ message: 'USER DELETED' })
        }

    } catch (e) {
        res.send(500).send({ error: e.message, message: 'SERVER ERROR' })
    }
}

const getUsers = async(req, res) => {
    try {

        const users = await User.find().select({ password: 0, __v: 0 })
        res.send({ data: users })

    } catch (e) {
        res.send(500).send({ error: e.message, message: 'SERVER ERROR' })
    }
}

const uploadImage = (req, res) => {
    try {

        const userId = req.params.id
        const file_name = 'Imagen NO subida'

        if (req.files) {
            const file_path = req.files.image.path

            const file_split = file_path.split('\\')
            const file_name = file_split[2]

            const ext_split = file_name.split('\.')
            const file_ext = ext_split[1]

            if (file_ext === 'png' || file_ext === 'jpg' || file_ext === 'gif') {

                User.findByIdAndUpdate(userId, { image: file_name }, (err, userUpdated) => {
                    if (err) {
                        res.status(500).send({ error: 'ERROR USER UPDATED' })
                    } else {
                        if (!userUpdated) {
                            res.status(404).send({ message: 'NO SE PUDO ACTUALIZAR EL USUARIO' })
                        } else {
                            console.log(`Se ha actualizado imagen en usuario: ${userId}`)
                            res.status(200).send({ image: file_name, user: userUpdated })
                        }
                    }
                })

            } else {
                res.status(200).send({ message: 'EXTENSIÓN INVÁLIDA, SOLO SE ADMITE .png .jpg .gif' })
            }

        } else {
            res.status(200).send({ message: 'NO has subido ninguna imagen' })
        }

    } catch (e) {
        res.status(500).send({ message: 'ERROR AL SUBIR IMAGEN' })
        console.log('Error upload image: ', e.message)
    }
}

const getImageFile = (req, res) => {
    try {

        const imageFile = req.params.imageFile
        const path_file = './uploads/users/' + imageFile

        fs.stat(path_file, (err, stat) => {
            if (err) {
                res.status(404).send({ message: 'NO EXISTE LA IMAGEN' });
            } else {
                res.sendFile(path.resolve(path_file));
            }
        })

    } catch (e) {
        res.status(500).send({ error: 'ERROR DE SERVIDOR' })
    }
}

// --- CREAR USUARIOS DE PRUEBA SIN PROCESO DE AUTENTICACIÓN VÍA EMAIL --- //
const saveStudent = async(req, res) => {
    try {
        const { username, email, password, password2 } = req.body

        if (username && email && password && password2) {

            if (password && password2) {

                if (password === password2) {

                    if (password2.length > 8) {

                        User.findOne({ email }).exec((err, user) => {
                            if (user) {
                                return res.status(400).json({ error: "email ya registrado, ingrese otro correo" })
                            } else {
                                function saveUser() {
                                    bcrypt.genSalt(10, (err, salt) => {
                                        if (err) throw err
                                        bcrypt.hash(password2, salt, async(err, hash) => {
                                            if (err) throw err

                                            const password = hash
                                            const role = 'ROLE_STU'
                                            const newUser = new User({ username, email, password, role })

                                            await newUser.save((err, userStored) => {
                                                if (err) {
                                                    if (err.code && err.code === 11000) {
                                                        console.log(err);
                                                        res.status(400).send({ message: 'DUPLICATE VALUES' });
                                                    }
                                                } else {
                                                    if (!userStored) {
                                                        res.status(404).send({ status: 'ERROR', message: 'User has not registered' });
                                                    } else {
                                                        console.log(userStored);
                                                        res.status(200).send({ user: userStored });
                                                    }
                                                }
                                            })
                                        })
                                    })
                                }
                                saveUser();
                            }
                        })

                    } else {
                        res.status(400).send({ message: 'PASS MUY CORTO' })
                    }

                } else {
                    res.status(400).send({ message: 'PASS NO SON IGUALES' })
                }

            } else {
                res.status(400).send({ message: 'INCOMPLETE DATA' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }

    } catch (e) {
        console.log('Error create user', e)
        res.status(500).send({ message: e.message })
    }
}

const saveProfessor = async(req, res) => {
    try {
        const { name, surname, username, email, telefono, pais, provincia, image, password, password2 } = req.body

        if (name && surname && username && email && telefono && pais && provincia && password && password2) {

            if (password && password2) {

                if (password === password2) {

                    if (password2.length > 8) {

                        function saveUser() {
                            bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err
                                bcrypt.hash(password2, salt, async(err, hash) => {
                                    if (err) throw err

                                    const password = hash
                                    const role = 'ROLE_PROF'
                                    const image = 'null'
                                    const newUser = new User({ name, surname, username, email, telefono, pais, provincia, image, password, role })

                                    await newUser.save((err, userStored) => {
                                        if (err) {
                                            if (err.code && err.code === 11000) {
                                                console.log(err);
                                                res.status(400).send({ message: 'DUPLICATE VALUES' });
                                            }
                                        } else {
                                            if (!userStored) {
                                                res.status(404).send({ status: 'ERROR', message: 'User has not registered' });
                                            } else {
                                                console.log(userStored);
                                                res.status(200).send({ user: userStored });
                                            }
                                        }
                                    })
                                })
                            })
                        }
                        saveUser();

                    } else {
                        res.status(400).send({ message: 'PASS MUY CORTO' })
                    }

                } else {
                    res.status(400).send({ message: 'PASS NO SON IGUALES' })
                }

            } else {
                res.status(400).send({ message: 'INCOMPLETE DATA' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }

    } catch (e) {
        console.log('Error create user', e)
        res.status(500).send({ message: e.message })
    }
    // try {
    //     const user = new User()
    //     const params = req.body

    //     user.name = params.name
    //     user.surname = params.surname
    //     user.username = params.username
    //     user.email = params.email
    //     user.telefono = params.telefono
    //     user.pais = params.pais
    //     user.provincia = params.provincia
    //     user.image = params.image
    //     user.role = 'ROLE_PROF'

    //     if (params.password) {
    //         const hash = await bcrypt.hash(params.password, 15)
    //         user.password = hash

    //         if (user.name && user.surname && user.username && user.email) {
    //             if (user.name != null && user.surname != null && user.username != null && user.email != null) {
    //                 await user.save((err, userStored) => {
    //                     if (err) {
    //                         if (err.code && err.code === 11000) {
    //                             console.log(err)
    //                             res.status(400).send({ message: 'DUPLICATE VALUES' })
    //                         }
    //                     } else {
    //                         if (!userStored) {
    //                             res.status(404).send({ message: 'User has not registered' })
    //                         } else {
    //                             console.log(`User registered: ${userStored}`)
    //                             res.status(200).send({ user: userStored })
    //                         }
    //                     }
    //                 })
    //             } else {
    //                 res.status(200).send({ message: 'INCOMPLETE PARAMETERS' })
    //             }
    //         } else {
    //             res.status(200).send({ message: 'INCOMPLETE DATA' })
    //         }

    //     } else {
    //         res.status(200).send({ message: 'ENTER A PASSWORD' })
    //     }
    // } catch (e) {
    //     console.log('Erro create teacher user')
    //     res.status(500).send({ message: e.message })
    // }
}

//-- Solo creará una cuenta de Administrador por única vez --//
const saveAdmin = async(req, res) => {
    try {

        const { password, password2 } = req.body

        if (password && password2) {

            if (password === password2) {

                if (password2.length > 8) {

                    function saveUser() {
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err) throw err
                            bcrypt.hash(password2, salt, async(err, hash) => {
                                if (err) throw err

                                const password = hash
                                const role = 'ROLE_ADMIN'
                                const email = 'admin@admin'
                                const username = 'admin'
                                const newUser = new User({ username, email, password, role })

                                await newUser.save((err, userStored) => {
                                    if (err) {
                                        if (err.code && err.code === 11000) {
                                            console.log(err);
                                            res.status(400).send({ message: 'El usuario admin ya existe, solicite nueva password a soporte técnico.' });
                                        }
                                    } else {
                                        if (!userStored) {
                                            res.status(404).send({ status: 'ERROR', message: 'User has not registered' });
                                        } else {
                                            console.log(userStored);
                                            res.status(200).send({ user: userStored });
                                        }
                                    }
                                })
                            })
                        })
                    }
                    saveUser();

                } else {
                    res.status(400).send({ message: 'PASS MUY CORTO' })
                }

            } else {
                res.status(400).send({ message: 'PASS NO SON IGUALES' })
            }

        } else {
            res.status(400).send({ message: 'INCOMPLETE DATA' })
        }

    } catch (e) {
        console.log('Error create user', e)
        res.status(500).send({ message: e.message })
    }
}

module.exports = {
    updateUser,
    deleteUser,
    getUsers,
    uploadImage,
    getImageFile,
    saveStudent,
    saveProfessor,
    saveAdmin
}