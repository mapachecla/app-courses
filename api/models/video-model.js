'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const VideoSchema = new Schema({
    name: { type: String, required: true },
    video_number: { type: Number },
    duration: { type: Number },
    file: { type: String },
    section: { type: String },
    section_description: { type: String },
    comments: { type: String },
    course: { type: Schema.ObjectId, ref: 'Course', required: true },
}, { timestamps: true })

module.exports = mongoose.model('Video', VideoSchema)