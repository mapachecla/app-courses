'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const CourseSchema = new Schema({
    title: { type: String, trim: true, required: true, max: 64 },
    about: { type: String, max: 64 },
    number_class: { type: Number, max: 9999999 },
    number_hours: { type: Number, max: 9999999 },
    language: { type: String, max: 14 },
    subtitle: { type: Boolean },
    description: { type: String, max: 64 },
    price: { type: Number, max: 9999999999 },
    price_discount: { type: Number, max: 99999 },
    image: { type: String },
    platform: { type: String, max: 34 },
    professor_description: { type: String, max: 64 },
    professor: { type: Schema.ObjectId, ref: 'User', required: true },
}, { timestamps: true })

module.exports = mongoose.model('Course', CourseSchema)