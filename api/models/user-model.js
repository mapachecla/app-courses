'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const UserSchema = new Schema({
    username: { type: String, trim: true, required: true, unique: true, max: 64 },
    email: { type: String, trim: true, required: true, unique: true, lowercase: true },
    password: { type: String, required: true },
    name: { type: String, max: 64 },
    surname: { type: String, max: 64 },
    telefono: { type: Number, max: 99999999999999999999 },
    pais: { type: String, max: 64 },
    provincia: { type: String, max: 64 },
    image: { type: String },
    //course: { type: Schema.ObjectId, ref: 'Course', required: true },
    course: { type: Schema.ObjectId, ref: 'Course' },
    role: { type: String, enum: ['ROLE_ADMIN', 'ROLE_PROF', 'ROLE_STU'], default: 'ROLE_STU' },
    resetLink: { data: String, default: '' }
}, { timestamps: true })

module.exports = mongoose.model('User', UserSchema)

// agregué la linea 16 (probar sino eliminarla)