'use strict'

const mongoose = require('mongoose')
const { Schema } = mongoose

const DocumentSchema = new Schema({
    name: { type: String, required: true },
    section: { type: String },
    section_description: { type: String },
    file: { type: String },
    video: { type: Schema.ObjectId, ref: 'Video', required: true },
}, { timestamps: true })

module.exports = mongoose.model('Document', DocumentSchema)