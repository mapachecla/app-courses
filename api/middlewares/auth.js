const jwtoken = require('jsonwebtoken')
const jwt = require('jwt-simple')
const moment = require('moment')
const route = require('../routes/v1/user-route')

// --- Comprueba si el token es correcto ---//
const ensureAuth = (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' })
        }

        const token = req.headers.authorization.replace(/['"']+/g, '')

        try {
            var payload = jwt.decode(token, process.env.JWT_SECRET)
            if (payload.exp <= moment().unix()) {
                return res.status(401).send({ status: 'ERROR', message: 'EXPIRED TOKEN' })
            }
        } catch (e) {
            return res.status(404).send({ status: 'ERROR', message: 'INVALID TOKEN' })
        }

        req.user = payload
        next()

    } catch (e) {
        console.log('Error middleware ensureAuth', e)
    }
}

// --- Valido por token para que cada usuario actualice sus propios datos ---//
const ensureAuthByUser = (req, res, next) => {
    try {

        const { authorization } = req.headers
        if (authorization) {
            const data = jwtoken.verify(authorization, process.env.JWT_SECRET)
            console.log('jwt data es: ', data)
            req.sessionData = { userId: data.sub, role: data.role }
            next()
        } else {
            throw {
                code: 403,
                status: 'ACCESS DENIED',
                message: 'NO SE ENCONTRÓ NINGUN token en el header wacho'
            }
        }

    } catch (e) {
        res.status(e.code || 500).send({ error: 'ACCESS DENIED', message: 'No se pudo acceder a buscar el token' })
    }
}

// --- Validar solo por PROF --- //
const isProf = (req, res, next) => {
    try {

        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' })
        }

        const token = req.headers.authorization.replace(/['"']+/g, '')

        const payload = jwtoken.decode(token, process.env.JWT_SECRET)

        if (payload.role !== 'ROLE_PROF') {
            throw {
                code: 403,
                status: 'ACCESS DENIED',
                message: 'Invalid token'
            }
        } else {
            next()
        }

    } catch (e) {
        res.status(e.code || 500).send({ status: e.status || 'ERROR', message: e.message })
    }
}

// --- Validar solo por ADMIN --- //
const isAdmin = (req, res, next) => {
    try {

        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' })
        }

        const token = req.headers.authorization.replace(/['"']+/g, '')

        const payload = jwtoken.decode(token, process.env.JWT_SECRET)

        if (payload.role !== 'ROLE_ADMIN') {
            throw {
                code: 403,
                status: 'ACCESS DENIED',
                message: 'Invalid token'
            }
        } else {
            next()
        }

    } catch (e) {
        res.status(e.code || 500).send({ status: e.status || 'ERROR', message: e.message })
    }
}


module.exports = {
    ensureAuth,
    ensureAuthByUser,
    isProf,
    isAdmin
}