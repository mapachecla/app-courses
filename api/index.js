'use strict'
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const app = require('./app')

dotenv.config()

const routesV1 = require('./routes/v1')
routesV1(app)

const PORT = process.env.PORT || 5000

mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(() => {
    console.log('==> Connected to mongodb')
    app.listen(PORT, () => {
        console.log(`==> Running on port: ${PORT}`)
    })
}).catch(error => {
    console.log('!! MONGODB ERROR: ', error)
})